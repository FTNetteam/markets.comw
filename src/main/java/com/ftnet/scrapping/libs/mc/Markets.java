package com.ftnet.scrapping.libs.mc;

import com.ftnet.libs.propertiesloader.PropertiesLoader;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

public class Markets {
    private static PropertiesLoader propertiesLoader = new PropertiesLoader("markets.properties", Markets.class);

    private static WebDriver webDriver;
    private HomePage homePage;
    private LoginPage loginPage;
    private MainPage mainPage;
    private JavascriptExecutor js;

    public Markets(WebDriver webDriver) {
        this.webDriver = webDriver;

        homePage = new HomePage(webDriver, propertiesLoader);

        mainPage = new MainPage(webDriver, propertiesLoader);

        loginPage = new LoginPage(webDriver, propertiesLoader);

        js = (JavascriptExecutor) webDriver;

    }

    public Markets navigateToMarkets() {
//        webDriver.manage().window().setSize(new Dimension(1440, 900));

        webDriver.get(propertiesLoader.getStringProperty("url"));

        WebElement element = webDriver.findElement(By.xpath("//*[@id=\"signin-btn\"]"));

        fluentWaitClickable(webDriver, element);

        js.executeScript("arguments[0].click()", element);

        return this;
    }

    public Markets login(String username, String password) {
        fluentWaitVisibility(webDriver, By.xpath("//*[text() = 'Login']"));
        webDriver.findElement(By.xpath("//*[text() = 'Login']"));

        fluentWaitVisibility(webDriver, loginPage.getUsername());

        loginPage.inputUsername(username);
        loginPage.inputPassword(password);
        loginPage.submit();

        return this;
    }

    public Markets switchRealDemo(boolean type) {

        try {
            fluentWaitVisibility(webDriver, By.xpath("//*[text() = 'Complete Registration']"));
        } catch (Exception e) {

        }

        js.executeScript("function popupCloser() {\n" +
                "    if ((document.getElementsByClassName(\"popup-container\").length > 0) && !(document.getElementsByClassName(\"popup-container\")[0].innerHTML.includes(\"Generate\"))) {\n" +
                "        console.log(\"load\");\n" +
                "        document.querySelector(\".fn-cancel\").click();\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "window.addEventListener('load', popupCloser, false);\n" +
                "document.body.addEventListener('DOMSubtreeModified', popupCloser, false);");
        if (!type) {
            fluentWaitVisibility(webDriver, mainPage.getDemoReal());
            if (webDriver.findElement(mainPage.getDemoReal()).getAttribute("outerHTML").contains("checked")) {
                WebElement element = webDriver.findElement(mainPage.getSwitchDemoReal());
                fluentWaitClickable(webDriver, webDriver.findElement(mainPage.getSwitchDemoReal()));
                js.executeScript("arguments[0].click()", element);

                js.executeScript("function popupCloser() {\n" +
                        "    if ((document.getElementsByClassName(\"popup-container\").length > 0) && !(document.getElementsByClassName(\"popup-container\")[0].innerHTML.includes(\"Generate\"))) {\n" +
                        "        console.log(\"load\");\n" +
                        "        document.querySelector(\".fn-cancel\").click();\n" +
                        "    }\n" +
                        "}\n" +
                        "\n" +
                        "window.addEventListener('load', popupCloser, false);\n" +
                        "document.body.addEventListener('DOMSubtreeModified', popupCloser, false);");
            }
        }

        return this;
    }

    public Markets getToSwitchStatement() {


        fluentWaitVisibility(webDriver, mainPage.getOpenPositionsTab());

        fluentWaitVisibility(webDriver, mainPage.getMyAccount());
        webDriver.findElement(mainPage.getMyAccount()).click();

        fluentWaitVisibility(webDriver, mainPage.getAccountStatement());
        fluentWaitClickable(webDriver, webDriver.findElement(mainPage.getAccountStatement()));
        webDriver.findElement(mainPage.getAccountStatement()).click();

        fluentWaitVisibility(webDriver, mainPage.getSelectMenu());
//        webDriver.findElement(mainPage.getSelectMenu()).click();
        new Select(webDriver.findElement(mainPage.getSelectMenu())).selectByVisibleText(" All history ");

        fluentWaitVisibility(webDriver, mainPage.getGenerate());
        webDriver.findElement(mainPage.getGenerate()).click();


        return this;
    }

    public String getSummary() {
        changeTab();
        fluentWaitVisibility(webDriver, By.xpath("//*[@id=\"summary-tbls\"]"));
        return webDriver.findElement(By.xpath("//*[@id=\"summary-tbls\"]")).getAttribute("outerHTML");
    }

    public String getWorkingOrders() {
        String bodyText = webDriver.findElement(By.tagName("body")).getText();
        if (bodyText.contains("No Working Orders"))
            return "";
        else
            return webDriver.findElement(By.xpath("//*[@id=\"main-container\"]/table[2]")).getAttribute("outerHTML");
    }

    public String getOpenPositions() {
        String bodyText = webDriver.findElement(By.tagName("body")).getText();
        if (bodyText.contains("No transactions"))
            return "";
        else if (bodyText.contains("No Working Orders"))
            return webDriver.findElement(By.xpath("//*[@id=\"main-container\"]/table[2]")).getAttribute("outerHTML");
        else
            return webDriver.findElement(By.xpath("//*[@id=\"main-container\"]/table[3]")).getAttribute("outerHTML");
    }

    public String getBalanceDetails() {
        String bodyText = webDriver.findElement(By.tagName("body")).getText();
        String balanceDetails = "";

        if (bodyText.contains("No transactions") && bodyText.contains("No Working Orders")) {
            if (fluentWaitVisibilityBoolean(webDriver, By.xpath("//*[@id=\"main-container\"]/table[2]"))) {
                balanceDetails = webDriver.findElement(By.xpath("//*[@id=\"main-container\"]/table[2]")).getAttribute("outerHTML");
            }
        }
        else if (bodyText.contains("No transactions"))
            balanceDetails = webDriver.findElement(By.xpath("//*[@id=\"main-container\"]/table[3]")).getAttribute("outerHTML");
        else if (bodyText.contains("No Working Orders"))
            balanceDetails = webDriver.findElement(By.xpath("//*[@id=\"main-container\"]/table[3]")).getAttribute("outerHTML");
        else if (fluentWaitVisibilityBoolean(webDriver, By.xpath("//*[@id=\"main-container\"]/table[4]"))) {
            balanceDetails = webDriver.findElement(By.xpath("//*[@id=\"main-container\"]/table[4]")).getAttribute("outerHTML");
        }

        return balanceDetails;
    }

    public String getAssetType() {
        return webDriver.findElement(By.xpath("//*[@id=\"main-container\"]/table[1]/tbody/tr[5]/td[2]")).getAttribute("outerHTML");
    }

    private void changeTab() {
        Set<String> windowHandles = webDriver.getWindowHandles();
        String subWindow = null;
        Iterator<String> iterator = windowHandles.iterator();
        while (iterator.hasNext()) {
            subWindow = iterator.next();
        }
        webDriver.switchTo().window(subWindow);
    }

    private void fluentWaitVisibility(WebDriver webDriver, By elementPath) {
        FluentWait fluentWait = new FluentWait(webDriver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofMillis(50))
                .ignoring(NoSuchElementException.class);
        try {
            fluentWait.until(ExpectedConditions.visibilityOfElementLocated(elementPath));
        } catch (NoSuchElementException e) {

        }
    }

    private void fluentWaitClickable(WebDriver webDriver, WebElement elementPath) {
        FluentWait fluentWait = new FluentWait(webDriver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofMillis(50))
                .ignoring(NoSuchElementException.class);
        try {
            fluentWait.until(ExpectedConditions.elementToBeClickable(elementPath));
        } catch (NoSuchElementException e) {

        }
    }

    private boolean fluentWaitVisibilityBoolean(WebDriver webDriver, By elementPath) {
        FluentWait fluentWait = new FluentWait(webDriver)
                .withTimeout(Duration.ofSeconds(3))
                .pollingEvery(Duration.ofMillis(50))
                .ignoring(NoSuchElementException.class);
        try {
            fluentWait.until(ExpectedConditions.visibilityOfElementLocated(elementPath));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
