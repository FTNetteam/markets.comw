package com.ftnet.scrapping.libs.mc;

import com.ftnet.libs.propertiesloader.PropertiesLoader;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Getter
public class MainPage {

    private static PropertiesLoader propertiesLoader;
    private WebDriver webDriver;
    private By orders;
    private By closedPositionsTab;
    private By closedPositionsCheck;
    private By closedPositionsExpands;
    private By closedPositions;
    private By demoReal;
    private By switchDemoReal;
    private By openPositionsTab;
    private By openPositionsCheck;
    private By openPositions;
    private By ordersTab;
    private By ordersCheck;
    private By myAccount;
    private By accountStatement;
    private By selectMenu;
    private By allHistory;
    private By generate;

    public MainPage(WebDriver webDriver, PropertiesLoader propertiesLoader) {

        if (MainPage.propertiesLoader == null) {
            MainPage.propertiesLoader = propertiesLoader;
            demoReal = By.xpath(propertiesLoader.getStringProperty("demoReal"));
            switchDemoReal = By.xpath(propertiesLoader.getStringProperty("switchDemoReal"));
            openPositionsTab = By.xpath(propertiesLoader.getStringProperty("openPositionsTab"));
            openPositionsCheck = By.xpath(propertiesLoader.getStringProperty("openPositionsCheck"));
            openPositions = By.xpath(propertiesLoader.getStringProperty("openPositions"));
            ordersTab = By.xpath(propertiesLoader.getStringProperty("ordersTab"));
            ordersCheck = By.xpath(propertiesLoader.getStringProperty("ordersCheck"));
            orders = By.xpath(propertiesLoader.getStringProperty("orders"));
            closedPositionsTab = By.xpath(propertiesLoader.getStringProperty("closedPositionsTab"));
            closedPositionsCheck = By.xpath(propertiesLoader.getStringProperty("closedPositionsCheck"));
            closedPositionsExpands = By.xpath(propertiesLoader.getStringProperty("closedPositionsExpands"));
            closedPositions = By.xpath(propertiesLoader.getStringProperty("closedPositions"));
            myAccount = By.xpath(propertiesLoader.getStringProperty("myAccount"));
            accountStatement = By.xpath(propertiesLoader.getStringProperty("accountStatement"));
            selectMenu = By.xpath(propertiesLoader.getStringProperty("selectMenu"));
            allHistory = By.xpath(propertiesLoader.getStringProperty("allHistory"));
            generate = By.xpath(propertiesLoader.getStringProperty("generate"));

        }
        this.webDriver = webDriver;
    }
}
