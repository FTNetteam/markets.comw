package com.ftnet.scrapping.libs.mc;

import com.ftnet.libs.propertiesloader.PropertiesLoader;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Getter
public class HomePage {

    private WebDriver webDriver;
    private static PropertiesLoader propertiesLoader;
    private By login;

    public HomePage(WebDriver webDriver, PropertiesLoader propertiesLoader) {

        if (HomePage.propertiesLoader == null) {
            HomePage.propertiesLoader = propertiesLoader;
            login = By.xpath(propertiesLoader.getStringProperty("login"));

        }
        this.webDriver = webDriver;
    }

}
