package com.ftnet.scrapping.libs.mc;

import com.ftnet.libs.propertiesloader.PropertiesLoader;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Getter
public class LoginPage {

    private WebDriver webDriver;
    private static PropertiesLoader propertiesLoader;
    private By username;
    private By password;
    private By submit;

    public LoginPage(WebDriver webDriver, PropertiesLoader propertiesLoader) {

        if (LoginPage.propertiesLoader == null) {
            LoginPage.propertiesLoader = propertiesLoader;
            username = By.xpath(propertiesLoader.getStringProperty("username"));
            password = By.xpath(propertiesLoader.getStringProperty("password"));
            submit = By.xpath(propertiesLoader.getStringProperty("submit"));
        }

        this.webDriver = webDriver;
    }

    public void inputUsername(String email){
        webDriver.findElement(username).sendKeys(email);
    }

    public void inputPassword(String pass){
        webDriver.findElement(password).sendKeys(pass);
    }

    public void submit(){
        webDriver.findElement(submit).click();
    }

}
