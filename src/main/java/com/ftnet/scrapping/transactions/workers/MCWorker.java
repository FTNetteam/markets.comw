package com.ftnet.scrapping.transactions.workers;

import com.ftnet.libs.tasksandresults.adapters.SimpleRedissonCollectionProvider;
import com.ftnet.libs.tasksandresults.facades.CollectionsFacade;
import com.ftnet.libs.tasksandresults.facades.interfaces.ConverterFacade;
import com.ftnet.libs.tasksandresults.facades.interfaces.WorkerFacade;
import com.ftnet.models.rawresult.mc.MCRawResult;
import com.ftnet.scrapping.libs.mc.Markets;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MCWorker {

    private static WorkerFacade WorkerFacade = new CollectionsFacade(new SimpleRedissonCollectionProvider("mc", "localhost", 6379));


    //private static PropertiesLoader propertiesLoader = new PropertiesLoader("markets.properties", MCWorker.class);

    //get this env from OS
//    private final static String redisAddress = System.getenv("REDIS_ADDRESS");
//    private final static String redisAddress = propertiesLoader.getStringProperty("redisAddress");
//    private final static String redisIp = redisAddress.split(":")[0];
//    private final static int redisPort = Integer.parseInt(redisAddress.split(":")[1]);

    //  private final static WorkerFacade workerFacade = new CollectionsFacade(new SimpleRedissonCollectionProvider("mc", redisIp, redisPort));

    public static void main(String[] args) {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");

        ChromeDriver webDriver = new ChromeDriver(options);

        Markets nMarkets = new Markets(webDriver);


        String summary = nMarkets.navigateToMarkets()
                .login("nizulu@emailure.net", "Password123")
                .switchRealDemo(false)
                .getToSwitchStatement()
                .getSummary();

//        String summary = nMarkets.navigateToMarkets()
//                .login("yitaxav@mailtrix.net", "Password123")
//                .switchRealDemo(false)
//                .getToSwitchStatement()
//                .getSummary();

        String workingOrders = nMarkets.getWorkingOrders();

        String openPositions = nMarkets.getOpenPositions();

        String balanceDetails = nMarkets.getBalanceDetails();

        String assetType = nMarkets.getAssetType().replace("<td>","").replace("</td>","");

        WorkerFacade.addRawResult(MCRawResult.builder()
                .accountSummary(summary)
                .assetType(assetType)
                .openPositions(openPositions)
                .orders(workingOrders)
                .balanceDetails(balanceDetails)
                .build());

    }
}
